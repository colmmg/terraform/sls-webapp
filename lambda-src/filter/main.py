import json
import boto3
from botocore.exceptions import ClientError

def parseCookies(headers):
  parsedCookie = {}
  if headers.get('cookie'):
    for cookie in headers['cookie'][0]['value'].split(';'):
      if cookie:
        parts = cookie.split('=')
        parsedCookie[parts[0].strip()] = parts[1].strip()
  return parsedCookie

def handler(event, context):
  request = event['Records'][0]['cf']['request']
  requestedUri = request['uri']
  if requestedUri == "/":
    request['uri'] = "/home.html"
  elif requestedUri == "/login/":
    request['uri'] = "/login.html"
  print ("Requested URI: " + requestedUri)
  print ("Target URI: " + request['uri'])

  headers = request['headers']

  if requestedUri == "/":
    parsedCookies = parseCookies(headers)
    if parsedCookies and 'session-id' in parsedCookies:
      sessionid = parsedCookies['session-id']
      if validSessionId(sessionid):
        return request
    redirectUrl = "https://" + headers['host'][0]['value'] + "/login/"
    response = {
      'status': '302',
      'statusDescription': 'Found',
      'headers': {
        'location': [{
          'key': 'Location',
          'value': redirectUrl
        }]
      }
    }
    return response
  else:
    return request

def validSessionId(sessionid):
  try:
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb.Table('my-serverless-website-sessions')
    response = table.get_item(
      Key={
        'session-id': sessionid
      }
    )
    if response["Item"]["userName"]:
      return True
  except (ClientError, KeyError) as err:
    return False
  return False
