resource "aws_api_gateway_domain_name" "main" {
  certificate_arn = data.aws_acm_certificate.main.arn
  domain_name     = "api.${var.domain}"
  security_policy = "TLS_1_2"
}

resource "aws_route53_record" "api" {
  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.main.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.main.cloudfront_zone_id
  }
  name    = "api.${var.domain}"
  type    = "A"
  zone_id = data.aws_route53_zone.main.zone_id
}

data "aws_api_gateway_rest_api" "sls" {
  name = "my-serverless-website-backend"
}

resource "aws_api_gateway_base_path_mapping" "sls" {
  api_id      = data.aws_api_gateway_rest_api.sls.id
  stage_name  = "dev"
  domain_name = aws_api_gateway_domain_name.main.domain_name
}
