provider "aws" {
  region = var.region
}

data "aws_caller_identity" "current" {
}

data "aws_route53_zone" "main" {
  name = "${var.domain}."
}

data "aws_acm_certificate" "main" {
  domain   = "*.${var.domain}"
  statuses = ["ISSUED"]
}
