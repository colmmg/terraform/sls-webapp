# sls-webapp
A sample serverless web application running on AWS.

# Prerequisites
This code assumes you have a [Route 53 zone](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html) created, the domain of which you will need to provide as a variable during the `terraform apply` stage. It is also expected that you have a wildcarded [AWS ACM](https://aws.amazon.com/certificate-manager/) certificate for this domain.

You should also have deployed the code at [gitlab.com/colmmg/serverless/sls-webapp](https://gitlab.com/colmmg/serverless/sls-webapp) before deploying this code.

# Terraform Deploy Instructions
To deploy run:
```
terraform init
terraform apply
```

# References
The following articles were referenced in the creation of this code:
* [Set Up a Custom Domain Name for an API in API Gateway](https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-custom-domains.html)
* [Lambda@Edge Example Functions](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-examples.html)
