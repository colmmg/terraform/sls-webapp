{
  "Version": "2008-10-17",
  "Id": "PolicyForCloudFrontPrivateContent",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ${origin_access_id}"
      },
      "Action": "s3:GetObject",
      "Resource": "arn:aws:s3:::my-serverless-website-${aws_account_id}/*"
    }
  ]
}
