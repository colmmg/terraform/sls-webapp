<!DOCTYPE html>
<html>
<head>
<title>Captain's Log</title>
<style type="text/css">
body, html {
  height: 100%;
  margin: 0;
  font: 400 15px/1.8 "Lato", sans-serif;
  color: #777;
}

.bgimg {
  position: relative;
  opacity: 0.65;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

}
.bgimg {
  background-image: url("https://sls.${domain}/stars.jpg");
  height: 100%;
}

.caption {
  position: absolute;
  left: 0;
  top: 50%;
  width: 100%;
  text-align: center;
  color: white;
}

a:link, a:visited, a:hover, a:active {
  color: wheat;
  text-decoration: none;
}

table {
  background: black;
  width: 100%;
}
</style>
<script type="text/javascript">
function getLogs() {
  var session_id = getCookie("session-id");
  var homeUrl = "https://api.${domain}/home?session-id=" + session_id;
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
      if (xmlhttp.status == 200) {
        var myObj = JSON.parse(xmlhttp.responseText);
        var x, txt = "";
        txt += "<table border='1'><th>Stardate</th><th>Planet</th><th>Log</th>"
        for (x in myObj) {
          txt += "<tr><td>" + myObj[x].stardate + "</td><td>" + myObj[x].planet + "</td><td>" + myObj[x].log + "</td></tr>";
        }
        txt += "</table>"    
        document.getElementById("logs").innerHTML = txt;
      }
      else {
        alert('An unexpected error occurred!');
      }
    }
  };

  xmlhttp.open("GET", homeUrl, true);
  xmlhttp.send();
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
</script>
</head>
<body>
<div class="bgimg">
  
  <div class="caption">
    <div id="logs"></div>
    <div>Image by <a href="https://pixabay.com/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1853491">Pexels</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1853491">Pixabay</a></div><br>
  </div>
</div>
<script type="text/javascript">
  getLogs()
</script>
</body>
</html>
