<!DOCTYPE html>
<html>
<head>
<title>Captain's Log - Login</title>
<style type="text/css">
body, html {
  height: 100%;
  margin: 0;
  font: 400 15px/1.8 "Lato", sans-serif;
  color: #777;
}

.bgimg {
  position: relative;
  opacity: 0.65;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

}
.bgimg {
  background-image: url("https://sls.${domain}/stars.jpg");
  height: 100%;
}

.caption {
  position: absolute;
  left: 0;
  top: 40%;
  width: 100%;
  text-align: center;
  color: white;
}

a:link, a:visited, a:hover, a:active {
  color: wheat;
  text-decoration: none;
}

.form {
  width: 300px;
  margin: 0 auto;
}

.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
  background: #43A047;
}
</style>
<script type="text/javascript">
function login() {
  var username = document.getElementById("username").value;
  var password = document.getElementById("password").value;
  var loginUrl = "https://api.${domain}/login?username=" + username + "&password=" + password;
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
      if (xmlhttp.status == 200) {
        var session_id = xmlhttp.responseText.replace(/['"]+/g, '');
        setCookie("session-id", session_id, 1);
        window.location.href = "/";
      }
      else if (xmlhttp.status == 401) {
        alert('Invalid user name or password!');
      }
      else {
        alert('An unexpected error occurred!');
      }
    }
  };

  xmlhttp.open("POST", loginUrl, true);
  xmlhttp.send();
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
</script>
</head>
<body>

<div class="bgimg">
  
  <div class="caption">
  <div class="form">
    <form autocomplete="off" onsubmit="login();return false" class="login-form">
      <input id="username" type="text" placeholder="username"/>
      <input id="password" type="password" placeholder="password"/>
      <button>login</button>
      <p class="message">Image by <a href="https://pixabay.com/users/Pexels-2286921/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1853491">Pexels</a> from <a href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=1853491">Pixabay</a><br>HTML template from <a href="https://codepen.io/colorlib/pen/rxddKy">https://codepen.io/colorlib/pen/rxddKy</a></p>
    </form>
</div>
</body>
</html>
