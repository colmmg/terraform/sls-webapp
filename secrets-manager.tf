resource "aws_secretsmanager_secret" "kirk" {
  name = "my-serverless-website/kirk"
}

resource "aws_secretsmanager_secret_version" "kirk" {
  secret_id     = "${aws_secretsmanager_secret.kirk.id}"
  secret_string = "{\"Password\": \"KHAAAN!!!\"}"
}

resource "aws_secretsmanager_secret" "picard" {
  name = "my-serverless-website/picard"
}

resource "aws_secretsmanager_secret_version" "picard" {
  secret_id     = "${aws_secretsmanager_secret.picard.id}"
  secret_string = "{\"Password\": \"makeitso\"}"
}
