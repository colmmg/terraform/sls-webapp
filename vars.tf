variable "domain" {
  description = "The domain of your website."
}

variable "region" {
  default     = "us-east-1"
  description = "The AWS region."
}
