resource "aws_cloudfront_origin_access_identity" "my-serverless-website" {
  comment = "My serverless website access identity"
}

resource "aws_cloudfront_distribution" "my-serverless-website" {
  aliases = ["sls.${var.domain}"]
  custom_error_response {
    error_code         = "403"
    response_code      = "403"
    response_page_path = "/error.html"
  }
  default_cache_behavior {
    allowed_methods = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods  = ["GET", "HEAD"]
    default_ttl     = 0
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
    lambda_function_association {
      event_type   = "viewer-request"
      lambda_arn   = "${aws_lambda_function.filter.qualified_arn}"
      include_body = false
    }
    max_ttl                = 86400
    min_ttl                = 0
    target_origin_id       = "my-serverless-website"
    viewer_protocol_policy = "redirect-to-https"
  }
  depends_on      = ["aws_s3_bucket.my-serverless-website"]
  enabled         = true
  is_ipv6_enabled = true
  origin {
    domain_name = "my-serverless-website-${data.aws_caller_identity.current.account_id}.s3.amazonaws.com"
    origin_id   = "my-serverless-website"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.my-serverless-website.cloudfront_access_identity_path
    }
  }
  price_class = "PriceClass_All"
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    acm_certificate_arn      = data.aws_acm_certificate.main.arn
    minimum_protocol_version = "TLSv1.1_2016"
    ssl_support_method       = "sni-only"
  }
}

resource "aws_route53_record" "sls" {
  alias {
    evaluate_target_health = "false"
    name                   = aws_cloudfront_distribution.my-serverless-website.domain_name
    zone_id                = aws_cloudfront_distribution.my-serverless-website.hosted_zone_id
  }
  name    = "sls.${var.domain}"
  type    = "A"
  zone_id = data.aws_route53_zone.main.zone_id
}
