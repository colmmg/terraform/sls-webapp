data "aws_iam_policy" "lambda" {
  arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

data "aws_iam_policy_document" "lambda" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
        "edgelambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "lambda" {
  assume_role_policy = data.aws_iam_policy_document.lambda.json
  name               = "my-serverless-website-lambda-role"
  path               = "/service-role/"
}

resource "aws_iam_role_policy_attachment" "lambda" {
  policy_arn = data.aws_iam_policy.lambda.arn
  role       = aws_iam_role.lambda.name
}

data "template_file" "lambda-dynamodb-policy" {
  template = file("${path.module}/templates/lambda-dynamodb-policy.json.tpl")
  vars = {
    aws_account_id = data.aws_caller_identity.current.account_id
  }
}

resource "aws_iam_role_policy" "lambda-dynamodb-policy" {
  name   = "lambda-dynamodb-policy"
  policy = data.template_file.lambda-dynamodb-policy.rendered
  role   = aws_iam_role.lambda.id
}

data "archive_file" "filter" {
  output_path = ".lambda/filter.zip"
  source_dir  = "lambda-src/filter"
  type        = "zip"
}

resource "aws_lambda_function" "filter" {
  filename         = ".lambda/filter.zip"
  function_name    = "my-serverless-website-filter"
  handler          = "main.handler"
  publish          = "true"
  role             = aws_iam_role.lambda.arn
  runtime          = "python3.7"
  source_code_hash = data.archive_file.filter.output_base64sha256
}
