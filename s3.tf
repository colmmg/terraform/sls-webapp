resource "aws_s3_bucket" "my-serverless-website" {
  acl    = "private"
  bucket = "my-serverless-website-${data.aws_caller_identity.current.account_id}"
  region = var.region
}

data "template_file" "my-serverless-website" {
  template = file("${path.module}/templates/bucket-policy.json.tpl")
  vars = {
    aws_account_id   = data.aws_caller_identity.current.account_id
    origin_access_id = aws_cloudfront_origin_access_identity.my-serverless-website.id
  }
}

resource "aws_s3_bucket_policy" "my-serverless-website" {
  bucket = aws_s3_bucket.my-serverless-website.id
  policy = data.template_file.my-serverless-website.rendered
}

data "template_file" "home" {
  template = file("${path.module}/templates/home.html.tpl")
  vars = {
    domain = var.domain
  }
}

data "template_file" "login" {
  template = file("${path.module}/templates/login.html.tpl")
  vars = {
    domain = var.domain
  }
}

data "template_file" "error" {
  template = file("${path.module}/templates/error.html.tpl")
  vars = {
    domain = var.domain
  }
}

resource "aws_s3_bucket_object" "home" {
  bucket       = "my-serverless-website-${data.aws_caller_identity.current.account_id}"
  content      = data.template_file.home.rendered
  content_type = "text/html"
  depends_on   = ["aws_s3_bucket.my-serverless-website"]
  key          = "home.html"
}

resource "aws_s3_bucket_object" "login" {
  bucket       = "my-serverless-website-${data.aws_caller_identity.current.account_id}"
  content      = data.template_file.login.rendered
  content_type = "text/html"
  depends_on   = ["aws_s3_bucket.my-serverless-website"]
  key          = "login.html"
}

resource "aws_s3_bucket_object" "error" {
  bucket       = "my-serverless-website-${data.aws_caller_identity.current.account_id}"
  content      = data.template_file.error.rendered
  content_type = "text/html"
  depends_on   = ["aws_s3_bucket.my-serverless-website"]
  key          = "error.html"
}

resource "aws_s3_bucket_object" "error-image" {
  bucket       = "my-serverless-website-${data.aws_caller_identity.current.account_id}"
  content_type = "image/jpeg"
  depends_on   = ["aws_s3_bucket.my-serverless-website"]
  key          = "error.jpg"
  source       = "files/error.jpg"
}

resource "aws_s3_bucket_object" "stars-image" {
  bucket       = "my-serverless-website-${data.aws_caller_identity.current.account_id}"
  content_type = "image/jpeg"
  depends_on   = ["aws_s3_bucket.my-serverless-website"]
  key          = "stars.jpg"
  source       = "files/stars.jpg"
}
