resource "aws_dynamodb_table" "my-serverless-website-sessions" {
  name           = "my-serverless-website-sessions"
  hash_key       = "session-id"
  read_capacity  = 5
  write_capacity = 5
  attribute {
    name = "session-id"
    type = "S"
  }
  ttl {
    attribute_name = "ttl"
    enabled        = true
  }
}

resource "aws_dynamodb_table" "my-serverless-website-logs" {
  name           = "my-serverless-website-logs"
  hash_key       = "userName"
  range_key      = "stardate"
  read_capacity  = 5
  write_capacity = 5
  attribute {
    name = "userName"
    type = "S"
  }
  attribute {
    name = "stardate"
    type = "S"
  }
}

resource "aws_dynamodb_table_item" "kirk1" {
  table_name = aws_dynamodb_table.my-serverless-website-logs.name
  hash_key   = aws_dynamodb_table.my-serverless-website-logs.hash_key
  range_key  = aws_dynamodb_table.my-serverless-website-logs.range_key

  item = <<ITEM
{
  "userName": {"S": "kirk"},
  "stardate": {"S": "2712.4"},
  "log": {"S": "A signal from planet Exo III. Dr. Roger Korby has been located, he and part of his expedition remaining alive due to the discovery of underground ruins left by the former inhabitants of this world."},
  "planet": {"S": "Exo III"}
}
ITEM
}

resource "aws_dynamodb_table_item" "kirk2" {
  table_name = aws_dynamodb_table.my-serverless-website-logs.name
  hash_key   = aws_dynamodb_table.my-serverless-website-logs.hash_key
  range_key  = aws_dynamodb_table.my-serverless-website-logs.range_key

  item = <<ITEM
{
  "userName": {"S": "kirk"},
  "stardate": {"S": "4523.3"},
  "log": {"S": "Deep Space Station K-7 has issued a priority one call. More than an emergency, it signals near or total disaster. We can only assume the Klingons have attacked the station. We're going in armed for battle."},
  "planet": {"S": "K-7"}
}
ITEM
}

resource "aws_dynamodb_table_item" "picard1" {
  table_name = aws_dynamodb_table.my-serverless-website-logs.name
  hash_key   = aws_dynamodb_table.my-serverless-website-logs.hash_key
  range_key  = aws_dynamodb_table.my-serverless-website-logs.range_key

  item = <<ITEM
{
  "userName": {"S": "picard"},
  "stardate": {"S": "41153.7"},
  "log": {"S": "Our destination is planet Deneb IV beyond which lies the great, unexplored mass of the galaxy. My orders are to examine Farpoint, a starbase built there by the inhabitants of that world."},
  "planet": {"S": "Deneb IV"}
}
ITEM
}

resource "aws_dynamodb_table_item" "picard2" {
  table_name = aws_dynamodb_table.my-serverless-website-logs.name
  hash_key   = aws_dynamodb_table.my-serverless-website-logs.hash_key
  range_key  = aws_dynamodb_table.my-serverless-website-logs.range_key

  item = <<ITEM
{
  "userName": {"S": "picard"},
  "stardate": {"S": "44474.5"},
  "log": {"S": "We have reached Ventax II and are attempting to contact the Federation science station, which at last report was under siege by an angry mob."},
  "planet": {"S": "Ventax II"}
}
ITEM
}
